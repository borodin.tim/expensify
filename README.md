Expensify App

Created within the Udemy React course

Create a list of tasks to do and let the computer choose what task you should do next.

Uses: React, webpack, babel, react modal, scss




To run server on local: yarn run dev-server
To run tests: yarn test --watch
To push changes to heroku: git push heroku master